DROP TABLE IF EXISTS `employee_mp`;
CREATE TABLE `employee_mp`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `name` varchar(50)  NOT NULL DEFAULT 'oop',
                                `age` smallint(4) NULL DEFAULT NULL,
                                `gender` char(1)  NULL DEFAULT NULL,
                                `motto` varchar(255)  NULL DEFAULT NULL,
                                `birthday` varchar(255)  NULL DEFAULT NULL,
                                `hobby` varchar(255)  NULL DEFAULT NULL,
                                PRIMARY KEY (`id`) USING BTREE
);