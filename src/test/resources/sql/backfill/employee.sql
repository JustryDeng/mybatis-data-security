DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `name` varchar(50)  NOT NULL DEFAULT 'oop',
                             `age` smallint(4) NULL DEFAULT NULL,
                             `gender` char(1)  NULL DEFAULT NULL,
                             `motto` varchar(255)  NULL DEFAULT NULL,
                             `birthday` datetime NULL DEFAULT NULL,
                             `hobby` varchar(255)  NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
);