package com.ideaaedi.mybatis.data.security.test.mybatisplus.model;

import com.ideaaedi.mybatis.data.security.support.PojoCloneable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

/**
 * 与EmployeeMp唯一不同是  实现了PojoCloneable支持
 */
@Getter
@Setter
@ToString(callSuper = true)
public class EmployeeMpClone extends EmployeeMp implements PojoCloneable<EmployeeMpClone> {
    
    @Override
    public EmployeeMpClone clonePojo() {
        EmployeeMpClone employeeMp = new EmployeeMpClone();
        BeanUtils.copyProperties(this, employeeMp);
        return employeeMp;
    }
}
