package com.ideaaedi.mybatis.data.security.test.mybatis.model;

import com.ideaaedi.mybatis.data.security.annotation.Encrypt;
import com.ideaaedi.mybatis.data.security.support.PojoCloneable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

/**
 * P.S. 除了实现了PojoCloneable外，其余地方和Employee一模一样
 *
 * @author JustryDeng
 * @since 2019/6/21 16:42
 */
//@EqualsAndHashCode
@Setter
@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloneSupportEmployee implements PojoCloneable<CloneSupportEmployee> {
    
    /** 用户id */
    private Integer id;
    
    /** 名字 */
    @Encrypt
    private String name;
    
    /** 年龄 */
    private Integer age;
    
    /** 性别 */
    private String gender;
    
    /** 座右铭 */
    @Encrypt
    private String motto;
    
    /** 生日 */
    private String birthday;
    
    /** 爱好 */
    @Encrypt
    private String hobby;
    
    @Override
    public CloneSupportEmployee clonePojo() {
        CloneSupportEmployee pojo = new CloneSupportEmployee();
        BeanUtils.copyProperties(this, pojo);
        return pojo;
    }
}
