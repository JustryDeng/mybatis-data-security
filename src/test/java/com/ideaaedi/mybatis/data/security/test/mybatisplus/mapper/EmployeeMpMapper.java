package com.ideaaedi.mybatis.data.security.test.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMpMapper extends BaseMapper<EmployeeMp> {

}
