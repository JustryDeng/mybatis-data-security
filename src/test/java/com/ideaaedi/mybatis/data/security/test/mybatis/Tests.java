package com.ideaaedi.mybatis.data.security.test.mybatis;

import com.ideaaedi.mybatis.data.security.TestApplication;
import com.ideaaedi.mybatis.data.security.config.MyEncryptExecutor;
import com.ideaaedi.mybatis.data.security.test.mybatis.mapper.AbcMapper;
import com.ideaaedi.mybatis.data.security.test.mybatis.mapper.XyzMapper;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.CloneSupportEmployee;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.Employee;
import com.ideaaedi.mybatis.data.security.util.AesUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 测试类
 * <p>
 *     提示：请激活配置文件：application-default.properties进行测试
 * </p>
 *
 * @author JustryDeng
 * @since 2021/7/18 17:27:02
 */
@SpringBootTest(classes = {TestApplication.class})
class Tests {
    
    @Resource
    AbcMapper abcMapper;
    
    @Resource
    XyzMapper xyzMapper;
    
    @Resource
    MyEncryptExecutor myEncryptExecutor;
    
    Random random = new SecureRandom();
    
    @Test
    public void testOne() {
        Employee u = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.insertOne(u);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testOne】未解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testOne】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        System.err.println("\n【testOne】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
    }
    
    @Test
    public void testTwo() {
        Employee u = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.insertTwo(u);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testTwo】未解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testTwo】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        System.err.println("\n【testTwo】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
    }
    
    @Test
    public void testThree() {
        int id = random.nextInt(Integer.MAX_VALUE);
        abcMapper.deleteByName(Employee.builder().id(id).name("JustryDeng").build());
        abcMapper.insertThree(id, "JustryDeng");
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testThree】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testThree】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testThree】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testSix() {
        int id = 123742;
        abcMapper.insertSix(id, "JustryDeng", 25);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testSix】未解密查询出来的数据为\n" + abcMapper.selectOneById(id));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testSix】解密查询出来的数据为\n" + abcMapper.selectOneById(id));
        System.err.println("\n【testSix】解密查询出来的数据为\n" + abcMapper.selectOneById(id));
    }
    
    @Test
    public void testEight() {
        Map<String, Employee> paramsMap = new HashMap<>();
        paramsMap.put("u", Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build());
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.insertEight(paramsMap);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testEight】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testEight】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testEight】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testNine() {
        Map<String, Employee> paramsMap = new HashMap<>();
        paramsMap.put("u", Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build());
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.insertNine(paramsMap);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testNine】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testNine】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testNine】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testTen() {
        List<Employee> data = new ArrayList<>(3);
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        Employee u2 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(26).gender("男").name("邓沙利文")
                .motto("这里的山路十八弯~").birthday("2020-02-02 22:26:00")
                .hobby("敲代码").build();
        Employee u3 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(27).gender("男").name("亨得帅")
                .motto("那就是青藏高原~").birthday("2020-02-03 22:26:00")
                .hobby("写代码").build();
        
        data.add(u1);
        data.add(u2);
        data.add(u3);
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.deleteByName(Employee.builder().name("邓沙利文").build());
        abcMapper.deleteByName(Employee.builder().name("亨得帅").build());
        abcMapper.insertTen(data);
        
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testTen】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testTen】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testTen】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testEleven() {
        List<Employee> data = new ArrayList<>(3);
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        Employee u2 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(26).gender("男").name("邓沙利文")
                .motto("这里的山路十八弯~").birthday("2020-02-02 22:26:00")
                .hobby("敲代码").build();
        Employee u3 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(27).gender("男").name("亨得帅")
                .motto("那就是青藏高原~").birthday("2020-02-03 22:26:00")
                .hobby("写代码").build();
        
        data.add(u1);
        data.add(u2);
        data.add(u3);
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.deleteByName(Employee.builder().name("邓沙利文").build());
        abcMapper.deleteByName(Employee.builder().name("亨得帅").build());
        abcMapper.insertEleven(data);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testEleven】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testEleven】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testEleven】解密查询出来的数据为\n" + abcMapper.selectAll());
        
    }
    
    @Test
    public void testTwelve() {
        Employee[] data = new Employee[3];
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        Employee u2 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(26).gender("男").name("邓沙利文")
                .motto("这里的山路十八弯~").birthday("2020-02-02 22:26:00")
                .hobby("敲代码").build();
        Employee u3 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(27).gender("男").name("亨得帅")
                .motto("那就是青藏高原~").birthday("2020-02-03 22:26:00")
                .hobby("写代码").build();
        
        data[0] = u1;
        data[1] = u2;
        data[2] = u3;
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.deleteByName(Employee.builder().name("邓沙利文").build());
        abcMapper.deleteByName(Employee.builder().name("亨得帅").build());
        abcMapper.insertTwelve(data);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testTwelve】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testTwelve】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testTwelve】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testThirteen() {
        Employee u = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        abcMapper.insertOne(u);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testThirteen】未解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testThirteen】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
        System.err.println("\n【testThirteen】解密查询出来的数据为\n" + abcMapper.selectOneById(u.getId()));
    }
    
    @Test
    public void testFourteen() {
        Employee[] data = new Employee[3];
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        Employee u2 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(26).gender("男").name("邓沙利文")
                .motto("这里的山路十八弯~").birthday("2020-02-02 22:26:00")
                .hobby("敲代码").build();
        Employee u3 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(27).gender("男").name("亨得帅")
                .motto("那就是青藏高原~").birthday("2020-02-03 22:26:00")
                .hobby("写代码").build();
        
        data[0] = u1;
        data[1] = u2;
        data[2] = u3;
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.deleteByName(Employee.builder().name("邓沙利文").build());
        abcMapper.deleteByName(Employee.builder().name("亨得帅").build());
        abcMapper.insertTwelve(data);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testFourteen】未解密查询出来的数据为\n" + abcMapper.selectAll());
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + abcMapper.selectAll());
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + abcMapper.selectAll());
    }
    
    @Test
    public void testFourteen2() {
        Employee[] data = new Employee[3];
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        Employee u2 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(26).gender("男").name("邓沙利文")
                .motto("这里的山路十八弯~").birthday("2020-02-02 22:26:00")
                .hobby("敲代码").build();
        Employee u3 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(27).gender("男").name("亨得帅")
                .motto("那就是青藏高原~").birthday("2020-02-03 22:26:00")
                .hobby("写代码").build();
        
        data[0] = u1;
        data[1] = u2;
        data[2] = u3;
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.deleteByName(Employee.builder().name("邓沙利文").build());
        abcMapper.deleteByName(Employee.builder().name("亨得帅").build());
        abcMapper.insertTwelve(data);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testFourteen】未解密查询出来的数据为\n" + Arrays.toString(abcMapper.selectAllAsArray()));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + Arrays.toString(abcMapper.selectAllAsArray()));
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + Arrays.toString(abcMapper.selectAllAsArray()));
    }
    
    @Test
    public void testSixteen() {
        Employee[] data = new Employee[1];
        Employee u1 = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        
        data[0] = u1;
        abcMapper.deleteByName(Employee.builder().name("JustryDeng").build());
        abcMapper.insertTwelve(data);
        
        myEncryptExecutor.setActiveEncryptExecutor(false);
        System.err.println("\n【testFourteen】未解密查询出来的数据为\n" + abcMapper.selectMapByName(AesUtil.encrypt("JustryDeng")));
        myEncryptExecutor.setActiveEncryptExecutor(true);
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + abcMapper.selectMapByName("JustryDeng"));
        System.err.println("\n【testFourteen】解密查询出来的数据为\n" + abcMapper.selectMapByName("JustryDeng"));
    }
    
    // ---------------------------------------------- 克隆对比测试 ----------------------------------------------
    
    /**
     * 测试连续解密查询数据（P.S. 主要是为了测试mybatis缓存）
     */
    @Test
    public void testCloneOne() {
        Employee u = Employee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        abcMapper.insertOne(u);
        System.err.println("\n【Employee不支持clone】入库后，源对象为：\n" + u);
        
        CloneSupportEmployee k = CloneSupportEmployee.builder()
                .id(random.nextInt(Integer.MAX_VALUE))
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        xyzMapper.insertOne(k);
        System.err.println("\n【CloneSupportEmployee支持clone】入库后，源对象为：\n" + k);
    }
    
    /**
     * 测试连续解密查询数据（P.S. 主要是为了测试mybatis缓存）
     */
    @Test
    public void testCloneTwo() {
        int uId = random.nextInt(Integer.MAX_VALUE);
        Employee u = Employee.builder()
                .id(uId)
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        abcMapper.insertOne(u);
        Employee employee = abcMapper.selectOneById(uId);
        System.err.println("\n【Employee不支持clone】第一次解密查询出来的数据为\n" + employee);
        employee.setMotto("蚂蚁牙黑~蚂蚁牙黑!黑!!!");
        System.err.println("\n【Employee不支持clone】第二次解密查询出来的数据为\n" + abcMapper.selectOneById(uId));
    
        
        int kId = random.nextInt(Integer.MAX_VALUE);
        CloneSupportEmployee k = CloneSupportEmployee.builder()
                .id(kId)
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！").birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        xyzMapper.insertOne(k);
        CloneSupportEmployee cloneSupportEmployee = xyzMapper.selectOneById(kId);
        System.err.println("\n【CloneSupportEmployee支持clone】第一次解密查询出来的数据为\n" + cloneSupportEmployee);
        cloneSupportEmployee.setMotto("蚂蚁牙黑~蚂蚁牙黑!黑!!!");
        System.err.println("\n【CloneSupportEmployee支持clone】第二次解密查询出来的数据为\n" + xyzMapper.selectOneById(kId));
    }
}
