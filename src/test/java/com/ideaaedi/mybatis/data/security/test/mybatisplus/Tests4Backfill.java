package com.ideaaedi.mybatis.data.security.test.mybatisplus;

import com.ideaaedi.mybatis.data.security.TestApplication;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMp;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMpClone;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.service.EmployeeMpService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 回填测试类
 * <p>
 * 提示1：请激活配置文件：application-backfill.properties进行测试
 * </p>
 *
 * @author JustryDeng
 * @since 2021/7/18 17:27:02
 */
@SpringBootTest(classes = {TestApplication.class})
class Tests4Backfill {
    
    @Resource
    EmployeeMpService employeeMpService;
    
    /**
     * 提示2：测试前请将{@link EmployeeMp#getId()}上的id策略设置为： @TableId(type = IdType.AUTO)
     */
    @Test
    public void testBackfill() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
        
        System.err.println("【testBackfill】回填id值为：" + employeeMp.getId());
        Assertions.assertNotNull(employeeMp.getId(), "回填id失败");
    }
    
    @Test
    public void testBackfill2() {
        EmployeeMp u = new EmployeeMp();
        u.setAge(100);
        u.setMotto("我是一只小小小小鸟~");
        u.setHobby("abcdefg");
        u.setName("张三");
        u.setBirthday("2023-10-09");
        u.setGender("男");
        
        
        List<EmployeeMp> list = new ArrayList<>();
        list.add(u);
        list.add(u);
        list.add(u);
        
        
        /*
         * mp的批量插入，和{@link com.ideaaedi.mybatis.data.security.test.mybatis.mapper.provider.AbcMapperProvider.insertTenProvider}这里的不一样，
         * mp的批量插入更像是逐条插入，所以：
         *     employeeMpService.saveBatch后，通用结论: 数据库中同时插入了3条数据,除了id不同外，加密字段也不同（一条比一条长），即：产生了重复加密
         */
        employeeMpService.saveBatch(list);
        for (EmployeeMp employee : list) {
            System.err.println(employee);
        }
        
    }
    
    /**
     * 与testBackfill2相似， 不过testBackfill2中的list是同一个对象 u, 而这里是不同的对象（只是这些对象与u的值一样）
     * <p>
     * 注：与testBackfill2的回填结果不一样，这里回填回来的id是各自不同的（因为本来就是不同的对象）
     */
    @Test
    public void testBackfill3() {
        EmployeeMp u = new EmployeeMp();
        u.setAge(100);
        u.setMotto("我是一只小小小小鸟~");
        u.setHobby("abcdefg");
        u.setName("张三");
        u.setBirthday("2023-10-09");
        u.setGender("男");
        
        
        List<EmployeeMp> list = new ArrayList<>();
        list.add(u);
    
        EmployeeMp otherU = new EmployeeMp();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        otherU = new EmployeeMp();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
    
        employeeMpService.saveBatch(list);
        
        for (EmployeeMp employee : list) {
            System.out.println(employee);
        }
        
    }
    
    // ---------------------------------------------- 克隆对比测试 ----------------------------------------------
    
    /**
     * 提示2：测试前请将{@link EmployeeMp#getId()}上的id策略设置为： @TableId(type = IdType.AUTO)
     */
    @Test
    public void testCloneBackfill() {
        EmployeeMpClone employeeMpClone = new EmployeeMpClone();
        employeeMpClone.setAge(100);
        employeeMpClone.setMotto("我是一只小小小小鸟~");
        employeeMpClone.setHobby("abcdefg");
        employeeMpClone.setName("张三");
        employeeMpClone.setBirthday("2020-10-06");
        employeeMpClone.setGender("男");
        employeeMpService.save(employeeMpClone);
        
        System.err.println("【testCloneBackfill】回填后对象值为：" + employeeMpClone);
        Assertions.assertNotNull(employeeMpClone.getId(), "回填id失败");
    }
    
    @Test
    public void testCloneBackfil2() {
        EmployeeMpClone u = new EmployeeMpClone();
        u.setAge(100);
        u.setMotto("我是一只小小小小鸟~");
        u.setHobby("abcdefg");
        u.setName("张三");
        u.setBirthday("2023-10-09");
        u.setGender("男");
        
        
        List<EmployeeMp> list = new ArrayList<>();
        list.add(u);
        list.add(u);
        list.add(u);
        
        
        /*
         * 因为采用了clone的方式，所以
         *     结论: 数据库中同时插入了3条数据,除了id不同,其余的值都相同，即：没有导致重复加密的问题
         */
        employeeMpService.saveBatch(list);
        for (EmployeeMp employee : list) {
            System.err.println(employee);
        }
        
    }
    
    /**
     * 与testCloneBackfill2相似， 不过testCloneBackfill2中的list是同一个对象 u, 而这里是不同的对象（只是这些对象与u的值一样）
     * <p>
     * 注：与testCloneBackfill2的回填结果不一样，这里回填回来的id是各自不同的（因为本来就是不同的对象）
     */
    @Test
    public void testCloneBackfill3() {
        EmployeeMpClone u = new EmployeeMpClone();
        u.setAge(100);
        u.setMotto("我是一只小小小小鸟~");
        u.setHobby("abcdefg");
        u.setName("张三");
        u.setBirthday("2023-10-09");
        u.setGender("男");
        
        
        List<EmployeeMp> list = new ArrayList<>();
        list.add(u);
    
        EmployeeMpClone otherU = new EmployeeMpClone();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        otherU = new EmployeeMpClone();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
    
        employeeMpService.saveBatch(list);
        
        for (EmployeeMp employee : list) {
            System.out.println(employee);
        }
        
    }
}
