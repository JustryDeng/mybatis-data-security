package com.ideaaedi.mybatis.data.security.test.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ideaaedi.mybatis.data.security.TestApplication;
import com.ideaaedi.mybatis.data.security.config.MyEncryptExecutor;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMp;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMpClone;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.service.EmployeeMpService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 测试类
 * <p>
 *     提示：请激活配置文件：application-default.properties进行测试
 * </p>
 *
 * @author JustryDeng
 * @since 2021/7/18 17:27:02
 */
@SpringBootTest(classes = TestApplication.class)
class Tests {
    
    @Resource
    private EmployeeMpService employeeMpService;
    
    @Resource
    MyEncryptExecutor myEncryptExecutor;
    
    @Test
    public void savaAndList() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(10011);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
    
        employeeMp = new EmployeeMp();
        employeeMp.setId(10012);
        employeeMp.setAge(100);
        employeeMp.setMotto("蚂蚁牙黑~");
        employeeMp.setHobby("吃饭");
        employeeMp.setName("李四");
        employeeMp.setBirthday("2020-10-07");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
        
    
        employeeMp = new EmployeeMp();
        employeeMp.setId(10013);
        employeeMp.setAge(100);
        employeeMp.setMotto("行不行啊细狗");
        employeeMp.setHobby("睡觉");
        employeeMp.setName("王五");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("女");
        employeeMpService.save(employeeMp);
    
        myEncryptExecutor.setDecrypt(false);
        System.err.println("未解密查询出来的数据为");
        for (EmployeeMp familyInfoGp : employeeMpService.list()) {
            System.err.println(familyInfoGp);
        }
    
        try {
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myEncryptExecutor.setDecrypt(true);
        System.out.println("解密查询出来的数据为");
        for (EmployeeMp familyInfoGp : employeeMpService.list()) {
            System.out.println(familyInfoGp);
        }
    }
    
    @Test
    public void query1() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(11);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
    
        myEncryptExecutor.setDecrypt(false);
        EmployeeMp one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .eq(EmployeeMp::getName, "张三")
        );
        System.err.println("未解密查询出来的数据为\n" + one);
    
        try {
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        myEncryptExecutor.setDecrypt(true);
        one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .eq(EmployeeMp::getName, "张三")
        );
        System.out.println("解密查询出来的数据为\n" + one);
    }
    
    /**
     * 与{@link Tests#query2Clone()}对比
     */
    @Test
    public void query2() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(12);
        employeeMp.setAge(1254);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
        
        EmployeeMp entity = new EmployeeMp();
        entity.setGender("男");
        entity.setBirthday("2020-10-08");
        entity.setName("张三");
        entity.setAge(1254);
    
        int beforeHashCode = entity.hashCode();
        System.err.println("本次查询前， entity的值为\t" + beforeHashCode + "\t" + entity);
        EmployeeMp one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .setEntity(entity)
                
        );
        System.err.println("解密查询出来的数据为\n" + one);
        int currHashCode = entity.hashCode();
        Assertions.assertEquals(currHashCode, beforeHashCode); // 对象本身没变
        System.err.println("本次查询后， entity发生了变化\t" + entity); // 对象里面的值变了
    
        
        try {
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        myEncryptExecutor.setDecrypt(true);
        one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .setEntity(entity)
        );
        Assertions.assertEquals(entity.hashCode(), currHashCode); // 对象本身没变
        System.out.println("再次以entity进行查询，查询不到了，因为进行本次查询时entity的值(即：条件值)已经变了\n" + one); // 对象里面的值变了
    }
    
    /**
     * 与{@link Tests#query2()}对比，EmployeeMpClone克隆  PojoCloneable
     */
    @Test
    public void query2Clone() {
        EmployeeMpClone employeeMp = new EmployeeMpClone();
        employeeMp.setId(12111);
        employeeMp.setAge(1254);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
    
        EmployeeMpClone entity = new EmployeeMpClone();
        entity.setGender("男");
        entity.setBirthday("2020-10-08");
        entity.setName("张三");
        entity.setAge(1254);
        entity.setId(12111);
    
        int beforeHashCode = entity.hashCode();
        System.err.println("本次查询前， entity的值为\t" + "\t" + entity);
        myEncryptExecutor.setEncrypt(true);
        myEncryptExecutor.setDecrypt(false);
        EmployeeMp one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .setEntity(entity)
                
        );
    
        int currHashCode = entity.hashCode();
        Assertions.assertEquals(currHashCode, beforeHashCode); // 对象本身没变
        System.err.println("未解密查询出来的数据为\n" + one);
        System.err.println("本次查询后， entity的值为\t" + "\t" + entity); // 对象里面的值也没变
        
        try {
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        myEncryptExecutor.setDecrypt(true);
        one = employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .setEntity(entity)
        );
        System.out.println("解密查询出来的数据为\n" + one);
    
        Assertions.assertEquals(entity.hashCode(), currHashCode); // 对象本身没变
        System.err.println("EmployeeMp实现了PojoCloneable， 所以实体类没有变."  + "\t" + entity); // 对象里面的值也没变
    }
    
    @Test
    public void query3() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(10011123);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三123");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
    
    
        employeeMp = new EmployeeMp();
        employeeMp.setId(10011343);
        employeeMp.setAge(100);
        employeeMp.setMotto("行不行啊细狗");
        employeeMp.setHobby("睡觉");
        employeeMp.setName("王五");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("女");
        employeeMpService.save(employeeMp);
    
        myEncryptExecutor.setDecrypt(false);
        List<EmployeeMp> list = employeeMpService.list(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getName, "张三123", "王五")
        );
        System.err.println("未解密查询出来的数据为");
        for (EmployeeMp mp : list) {
            System.err.println(mp);
        }
    
        myEncryptExecutor.setDecrypt(true);
        list = employeeMpService.list(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getName, "张三123", "王五")
        );
        System.out.println("解密查询出来的数据为");
        for (EmployeeMp mp : list) {
            System.out.println(mp);
        }
    }
    
    /**
     * 分页插件测试
     */
    @Test
    public void query4() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(10011124);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三123");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
    
    
        employeeMp = new EmployeeMp();
        employeeMp.setId(10011345);
        employeeMp.setAge(100);
        employeeMp.setMotto("行不行啊细狗");
        employeeMp.setHobby("睡觉");
        employeeMp.setName("王五");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("女");
        employeeMpService.save(employeeMp);
        
    
        employeeMp = new EmployeeMp();
        employeeMp.setId(10011346);
        employeeMp.setAge(100);
        employeeMp.setMotto("行不行啊细狗");
        employeeMp.setHobby("睡觉");
        employeeMp.setName("王五");
        employeeMp.setBirthday("2020-10-08");
        employeeMp.setGender("女");
        employeeMpService.save(employeeMp);
    
        // 分页查
        myEncryptExecutor.setDecrypt(false);
        IPage<EmployeeMp> pageInfo = new Page<>(1, 1);
        IPage<EmployeeMp> page = employeeMpService.page(pageInfo, new LambdaQueryWrapper<>());

        System.err.println("未解密查询出来的数据为");
        for (EmployeeMp mp : page.getRecords()) {
            System.err.println(mp);
        }
    
    
        // 分页查
        myEncryptExecutor.setDecrypt(true);
        pageInfo = new Page<>(1, 1);
        page = employeeMpService.page(pageInfo,
                new LambdaQueryWrapper<EmployeeMp>()
                .eq(EmployeeMp::getName, "王五")
        );
    
        System.out.println("解密查询出来的数据为");
        for (EmployeeMp mp : page.getRecords()) {
            System.out.println(mp);
        }
    }
    
    @Test
    public void update() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(1236345);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
        
        employeeMpService.update(new LambdaUpdateWrapper<EmployeeMp>()
                .set(EmployeeMp::getName, "张三abc")
                .set(EmployeeMp::getGender, "女")
                .eq(EmployeeMp::getName, "张三")
                );
        myEncryptExecutor.setDecrypt(false);
        System.err.println("未解密查询出来的数据为\n" + employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getId, 1236345)));
   

        myEncryptExecutor.setDecrypt(true);
        System.out.println("解密查询出来的数据为\n" + employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getId, 1236345)));
        
        
        employeeMpService.update(new LambdaUpdateWrapper<EmployeeMp>()
                .set(EmployeeMp::getName, "张三")
                .set(EmployeeMp::getGender, "男")
                .eq(EmployeeMp::getName, "张三abc")
                );
    
        myEncryptExecutor.setDecrypt(false);
        System.err.println("未解密查询出来的数据为\n" + employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getId, 1236345)));
    
        
        myEncryptExecutor.setDecrypt(true);
        System.out.println("解密查询出来的数据为\n" + employeeMpService.getOne(new LambdaQueryWrapper<EmployeeMp>()
                .in(EmployeeMp::getId, 1236345)));
    }
    
    /**
     * 耗时计算（第一次不走数据库缓存； 第二次走数据库缓存，那么第二次的耗时大致就是加解密的耗时）
     */
    @Test
    public void getById() {
        EmployeeMp employeeMp = new EmployeeMp();
        employeeMp.setId(1);
        employeeMp.setAge(100);
        employeeMp.setMotto("我是一只小小小小鸟~");
        employeeMp.setHobby("abcdefg");
        employeeMp.setName("张三");
        employeeMp.setBirthday("2020-10-06");
        employeeMp.setGender("男");
        employeeMpService.save(employeeMp);
        
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("第一次");
        System.err.println(employeeMpService.getById(1));
        stopWatch.stop();
        stopWatch.start("第二次");
        System.err.println(employeeMpService.getById(1));
        stopWatch.stop();
        System.err.println("耗时" + stopWatch.prettyPrint());
    }
}