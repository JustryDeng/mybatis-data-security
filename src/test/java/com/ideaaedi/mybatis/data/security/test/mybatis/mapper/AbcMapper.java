package com.ideaaedi.mybatis.data.security.test.mybatis.mapper;

import com.ideaaedi.mybatis.data.security.annotation.Encrypt;
import com.ideaaedi.mybatis.data.security.test.mybatis.mapper.provider.AbcMapperProvider;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.CloneSupportEmployee;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.Employee;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 除了操作对象变为了CloneSupportEmployee外，其余地方和AbcMapper一模一样
 *
 * @author JustryDeng
 * @date 2018年6月13日下午11:36:37
 */
@Mapper
@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface AbcMapper {

    /**
     * 增------实体类 - 有@Param注解
     */
    @Insert("INSERT INTO employee (`id`,`name`, `age`, `gender`, `motto`, `birthday`, `hobby`) "
            + " VALUES(#{u.id},#{u.name},#{u.age},#{u.gender},#{u.motto},#{u.birthday},#{u.hobby})")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "u.id")
    int insertOne(@Param("u") Employee employee);

    /**
     * 增------实体类 - 无@Param注解
     */
    @Insert("INSERT INTO employee (`id`, `name`, `age`, `gender`, `motto`, `birthday`, `hobby`) "
            + " VALUES(#{id},#{name},#{age},#{gender},#{motto},#{birthday},#{hobby})")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    int insertTwo(Employee employee);

    /**
     * 增------普通字段
     */
    @Insert("INSERT INTO employee (`id`,`name`) VALUES(#{id}, #{name})")
    int insertThree(@Param("id")int id, @Param("name") @Encrypt String name);

    /**
     * 增------普通字段
     */
    @Insert("INSERT INTO employee (`id`, `name`, `age`) VALUES(#{id},#{name},#{age})")
    int insertSix(@Param("id")int id, @Encrypt @Param("name") String name, @Param("age") Integer age);

    /**
     * 增------Map  无@Param
     */
    @Insert("INSERT INTO employee (`id`, `name`, `age`) VALUES(#{u.id},#{u.name},#{u.age})")
    int insertEight(Map<String, Employee> paramsMap);
    
    /**
     * 增------Map  有@Param
     */
    @Insert("INSERT INTO employee (`id`,`name`, `age`) VALUES(#{p.u.id},#{p.u.name},#{p.u.age})")
    int insertNine(@Param("p") Map<String, Employee> paramsMap);

    /**
     * 批量增------集合  无@Param
     */
    @InsertProvider(type = AbcMapperProvider.class, method = "insertTenProvider")
    int insertTen(List<Employee> employeeList);

    /**
     * 批量增------集合  有@Param
     */
    @InsertProvider(type = AbcMapperProvider.class, method = "insertElevenProvider")
    int insertEleven(@Param("list") List<Employee> employeeList);
    
    /**
     * 批量增------数组  有@Param
     */
    @InsertProvider(type = AbcMapperProvider.class, method = "insertTwelveProvider")
    Integer insertTwelve(@Param("myArray") Employee[] employeeArray);

    /**
     * 根据id查询员工信息
     *
     * @param id
     *            id
     * @return  查询结果
     * @date 2020/2/5 23:31
     */
    @Select("SELECT `id`,`name`,`age`,`gender`,`motto`,`birthday`,`hobby` FROM `employee` WHERE id = #{id}")
    Employee selectOneById(@Param("id") Integer id);

    /**
     * 查询所有的员工信息 集合接收
     *
     * @return  查询结果
     * @date 2020/2/5 23:31
     */
    @Select("SELECT `id`,`name`,`age`,`gender`,`motto`,`birthday`,`hobby` FROM `employee` ")
    List<Employee> selectAll();
    
    /**
     * 查询所有的员工信息 数组接收
     *
     * @return  查询结果
     * @date 2020/2/5 23:31
     */
    @Select("SELECT `id`,`name`,`age`,`gender`,`motto`,`birthday`,`hobby` FROM `employee` ")
    Employee[] selectAllAsArray();
    
    /**
     * 根据name查询员工信息, 以Map<Integer, Employee>接收数据
     *
     * @param name
     *            员工姓名
     * @return  查询结果
     * @date 2020/2/5 23:31
     */
    @MapKey("id")
    @Select("SELECT `id`,`name`,`age`,`gender`,`motto`,`birthday`,`hobby` FROM `employee` WHERE name = #{name}")
    Map<Integer, Employee> selectMapByName(@Encrypt @Param("name") String name);
    
    
    @Delete("delete FROM `employee` WHERE name = #{name}")
    void deleteByName(Employee employee);
    
    /**
     * 测试: id回填
     */
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @InsertProvider(type = AbcMapperProvider.class, method = "insert4Test4OriginPojoAndEncryptedPojoMapProvider")
    int insert4Test4OriginPojoAndEncryptedPojoMap(@Param("list") List<Employee> employeeList);
    
    /**
     * 与{@link AbcMapper#insert4Test4OriginPojoAndEncryptedPojoMap}测试内容一样,不过这里主要测试的是clone对象
     */
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @InsertProvider(type = AbcMapperProvider.class, method = "insert4Test4Clone4OriginPojoAndEncryptedPojoMapProvider")
    int insert4Test4Clone4OriginPojoAndEncryptedPojoMap(@Param("list") List<CloneSupportEmployee> employeeList);
}