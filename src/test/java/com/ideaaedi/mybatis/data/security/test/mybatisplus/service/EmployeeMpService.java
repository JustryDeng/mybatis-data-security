package com.ideaaedi.mybatis.data.security.test.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMp;

public interface EmployeeMpService extends IService<EmployeeMp> {

}
