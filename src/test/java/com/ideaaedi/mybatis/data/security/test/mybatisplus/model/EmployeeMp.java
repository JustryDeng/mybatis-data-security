package com.ideaaedi.mybatis.data.security.test.mybatisplus.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ideaaedi.mybatis.data.security.annotation.Encrypt;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@TableName("employee_mp")
public class EmployeeMp implements Serializable {
    
    /** 用户id */
    @TableId(type = IdType.INPUT)
//  @TableId(type = IdType.AUTO) // 使用mybatis-plus进行回填测试时，将这个解开
    private Integer id;
    
    /** 名字 */
    @Encrypt
    @TableField("name")
    private String name;
    
    /** 年龄 */
    @TableField("age")
    private Integer age;
    
    /** 性别 */
    @TableField("gender")
    private String gender;
    
    /** 座右铭 */
    @Encrypt
    @TableField("motto")
    private String motto;
    
    /** 生日 */
    @Encrypt
    @TableField("birthday")
    private String birthday;
    
    /** 爱好 */
    @Encrypt
    @TableField("hobby")
    private String hobby;
}
