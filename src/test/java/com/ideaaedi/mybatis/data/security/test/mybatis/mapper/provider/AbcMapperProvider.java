package com.ideaaedi.mybatis.data.security.test.mybatis.mapper.provider;

import com.ideaaedi.mybatis.data.security.test.mybatis.model.CloneSupportEmployee;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sql provider
 *
 * @author JustryDeng
 * @date 2020/2/2 11:45
 */
@SuppressWarnings("all")
public class AbcMapperProvider {

    public String insertTenProvider(@Param("list") List<Employee> employeeList) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("INSERT INTO employee (`id`, `name`, `age`, `gender`, `motto`, `birthday`, `hobby`)");
        sb.append(" VALUES ");
        int size = employeeList.size();
        for (int i = 0; i < size; i++) {
            sb.append(" (");
            sb.append(" #{list[" + i + "].id}," )
              .append("#{list[" + i + "].name}," )
              .append("#{list[" + i + "].age}," )
              .append("#{list[" + i + "].gender}," )
              .append("#{list[" + i + "].motto}," )
              .append("#{list[" + i + "].birthday}," )
              .append("#{list[" + i + "].hobby}");
            sb.append(" )");
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public String insertElevenProvider(@Param("list") List<Employee> employeeList) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("INSERT INTO employee (`id`, `name`, `age`, `gender`, `motto`, `birthday`, `hobby`)");
        sb.append(" VALUES ");
        int size = employeeList.size();
        for (int i = 0; i < size; i++) {
            sb.append(" (");
            sb.append(" #{list[" + i + "].id}," )
                    .append("#{list[" + i + "].name}," )
                    .append("#{list[" + i + "].age}," )
                    .append("#{list[" + i + "].gender}," )
                    .append("#{list[" + i + "].motto}," )
                    .append("#{list[" + i + "].birthday}," )
                    .append("#{list[" + i + "].hobby}");
            sb.append(" )");
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public String insertTwelveProvider(@Param("myArray") Employee[] employeeArray) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("INSERT INTO employee (`id`,`name`, `age`, `gender`, `motto`, `birthday`, `hobby`)");
        sb.append(" VALUES ");
        int length = employeeArray.length;
        for (int i = 0; i < length; i++) {
            sb.append(" (");
            sb.append(" #{myArray[" + i + "].id}," )
                    .append("#{myArray[" + i + "].name}," )
                    .append("#{myArray[" + i + "].age}," )
                    .append("#{myArray[" + i + "].gender}," )
                    .append("#{myArray[" + i + "].motto}," )
                    .append("#{myArray[" + i + "].birthday}," )
                    .append("#{myArray[" + i + "].hobby}");
            sb.append(" )");
            if (i < length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
    public String insert4Test4OriginPojoAndEncryptedPojoMapProvider(@Param("list") List<Employee> employeeList) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("INSERT INTO employee (`name`, `age`, `gender`, `motto`, `birthday`, `hobby`)");
        sb.append(" VALUES ");
        int size = employeeList.size();
        for (int i = 0; i < size; i++) {
            sb.append(" (");
            sb.append("#{list[" + i + "].name}," )
                    .append("#{list[" + i + "].age}," )
                    .append("#{list[" + i + "].gender}," )
                    .append("#{list[" + i + "].motto}," )
                    .append("#{list[" + i + "].birthday}," )
                    .append("#{list[" + i + "].hobby}");
            sb.append(" )");
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
    public String insert4Test4Clone4OriginPojoAndEncryptedPojoMapProvider(@Param("list") List<CloneSupportEmployee> employeeList) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("INSERT INTO employee (`name`, `age`, `gender`, `motto`, `birthday`, `hobby`)");
        sb.append(" VALUES ");
        int size = employeeList.size();
        for (int i = 0; i < size; i++) {
            sb.append(" (");
            sb.append("#{list[" + i + "].name}," )
                    .append("#{list[" + i + "].age}," )
                    .append("#{list[" + i + "].gender}," )
                    .append("#{list[" + i + "].motto}," )
                    .append("#{list[" + i + "].birthday}," )
                    .append("#{list[" + i + "].hobby}");
            sb.append(" )");
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
