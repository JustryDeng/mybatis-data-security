package com.ideaaedi.mybatis.data.security;

import com.ideaaedi.mybatis.data.security.annotation.EnableMybatisDataSecurity;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableMybatisDataSecurity
@MapperScan(value = {"com.ideaaedi.mybatis.data.security.test.mybatisplus.mapper", "com.ideaaedi.mybatis.data.security.test.mybatis.mapper"})
public class TestApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
