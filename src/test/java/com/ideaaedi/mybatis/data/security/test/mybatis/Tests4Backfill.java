package com.ideaaedi.mybatis.data.security.test.mybatis;

import com.ideaaedi.mybatis.data.security.TestApplication;
import com.ideaaedi.mybatis.data.security.test.mybatis.mapper.AbcMapper;
import com.ideaaedi.mybatis.data.security.test.mybatis.mapper.XyzMapper;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.CloneSupportEmployee;
import com.ideaaedi.mybatis.data.security.test.mybatis.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 回填测试类
 * <p>
 * 请激活配置文件：application-backfill.properties进行测试
 * </p>
 *
 * @author JustryDeng
 * @since 2021/7/18 17:27:02
 */
@SpringBootTest(classes = {TestApplication.class})
class Tests4Backfill {
    
    @Resource
    AbcMapper abcMapper;
    
    @Resource
    XyzMapper xyzMapper;
    
    @Test
    public void testBackfill() {
        Employee u = Employee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2020-02-01 22:26:00")
                .hobby("撸代码")
                .build();
        abcMapper.insertOne(u);
        
        System.err.println("【testBackfill】回填后对象值为：" + u);
        Assertions.assertNotNull(u.getId(), "回填id失败");
    }
    
    @Test
    public void testBackfill2() {
        Employee u = Employee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2023-10-09 22:26:00")
                .hobby("撸代码")
                .build();
    
    
        List<Employee> list = new ArrayList<>();
        list.add(u);
        list.add(u);
        list.add(u);
        /*
         * --------------------------------------------- 批量插入
         * 基于批量插入：
         *     结论1: 数据库中同时插入了3条数据,除了id不同,其余的值都相同，即：没有导致重复加密的问题
         */
        abcMapper.insert4Test4OriginPojoAndEncryptedPojoMap(list);
    
        // 通用结论: 这个list中的对象（实际是同一个对象），仍然是同一个对象，id值是最后自增的那个id值
        for (Employee employee : list) {
            System.out.println(employee);
        }
        
        /*
         * --------------------------------------------- 逐条插入
         * 基于逐条插入：
         *     结论1: 数据库中同时插入了3条数据,除了id不同外，加密字段也不同（一条比一条长），即：产生了重复加密
         */
        for (Employee employee : list) {
            employee.setId(null); // 这里设置为null，是因为第一次插入成功后，会回写id，此时如果不设置为null就再次插入，则会因为id重复而导致报错
            abcMapper.insertOne(employee);
        }
    
        // 通用结论: 这个list中的对象（实际是同一个对象），仍然是同一个对象，id值是最后自增的那个id值
        for (Employee employee : list) {
            System.err.println(employee);
        }

    }
    
    /**
     * 与testBackfill2相似， 不过testBackfill2中的list是同一个对象 u, 而这里是不同的对象（只是这些对象与u的值一样）
     * <p>
     * 注：与testBackfill2的回填结果不一样，这里回填回来的id是各自不同的（因为本来就是不同的对象）
     */
    @Test
    public void testBackfill3() {
        Employee u = Employee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2023-10-09 22:26:00")
                .hobby("撸代码")
                .build();
        
        
        List<Employee> list = new ArrayList<>();
        list.add(u);
        
        Employee otherU = new Employee();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        otherU = new Employee();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        abcMapper.insert4Test4OriginPojoAndEncryptedPojoMap(list);
        
        for (Employee employee : list) {
            System.out.println(employee);
        }
        
    }
    
    // ---------------------------------------------- 克隆对比测试 ----------------------------------------------
    
    @Test
    public void testCloneBackfill() {
        CloneSupportEmployee k = CloneSupportEmployee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2020-02-01 22:26:00")
                .hobby("撸代码").build();
        
        xyzMapper.insertOne(k);
        System.err.println("【testCloneBackfill】回填后对象值为：" + k);
        Assertions.assertNotNull(k.getId(), "回填id失败");
    }
    
    @Test
    public void testCloneBackfill2() {
        CloneSupportEmployee u = CloneSupportEmployee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2023-10-09 22:26:00")
                .hobby("撸代码")
                .build();
        
        
        List<CloneSupportEmployee> list = new ArrayList<>();
        list.add(u);
        list.add(u);
        list.add(u);
        /*
         * --------------------------------------------- 批量插入
         * 基于批量插入：
         *     结论1: 数据库中同时插入了3条数据,除了id不同,其余的值都相同，即：没有导致重复加密的问题
         */
        abcMapper.insert4Test4Clone4OriginPojoAndEncryptedPojoMap(list);
    
        // 通用结论: 这个list中的对象（实际是同一个对象），仍然是同一个对象，id值是最后自增的那个id值
        for (CloneSupportEmployee e : list) {
            System.out.println(e);
        }
    
        /*
         * --------------------------------------------- 逐条插入
         * 基于逐条插入：
         *     结论1: 数据库中同时插入了3条数据,除了id不同,其余的值都相同，即：没有导致重复加密的问题
         */
        for (CloneSupportEmployee e : list) {
            e.setId(null); // 这里设置为null，是因为第一次插入成功后，会回写id，此时如果不设置为null就再次插入，则会因为id重复而导致报错
            xyzMapper.insertOne(e);
        }
    
        // 通用结论: 这个list中的对象（实际是同一个对象），仍然是同一个对象，id值是最后自增的那个id值
        for (CloneSupportEmployee e : list) {
            System.err.println(e);
        }
    }
    
    /**
     * 与testCloneBackfill2相似， 不过testCloneBackfill2中的list是同一个对象 u, 而这里是不同的对象（只是这些对象与u的值一样）
     * <p>
     * 注：与testCloneBackfill2的回填结果不一样，这里回填回来的id是各自不同的（因为本来就是不同的对象）
     */
    @Test
    public void testCloneBackfill3() {
        CloneSupportEmployee u = CloneSupportEmployee.builder()
                .age(25).gender("男").name("JustryDeng")
                .motto("我是一只小小小小鸟~嗷！嗷！")
                .birthday("2023-10-09 22:26:00")
                .hobby("撸代码")
                .build();
        
        
        List<CloneSupportEmployee> list = new ArrayList<>();
        list.add(u);
    
        CloneSupportEmployee otherU = new CloneSupportEmployee();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        otherU = new CloneSupportEmployee();
        BeanUtils.copyProperties(u, otherU);
        list.add(otherU);
        
        abcMapper.insert4Test4Clone4OriginPojoAndEncryptedPojoMap(list);
        
        for (CloneSupportEmployee employee : list) {
            System.out.println(employee);
        }
        
    }
}
