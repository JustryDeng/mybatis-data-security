package com.ideaaedi.mybatis.data.security.config;

import com.ideaaedi.mybatis.data.security.annotation.Encrypt;
import com.ideaaedi.mybatis.data.security.support.EncryptExecutor;
import com.ideaaedi.mybatis.data.security.util.AesUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * (non-javadoc)
 *
 * @author JustryDeng
 * @since 2021/2/11 19:08:05
 */
@Component
public class MyEncryptExecutor implements EncryptExecutor {

    @Setter
    @Getter
    private boolean activeEncryptExecutor = true;

    @Setter
    @Getter
    private boolean encrypt = true;

    @Setter
    @Getter
    private boolean decrypt = true;
    
    @Override
    public String encryptParameter(String paramName, String paramValue, Encrypt annotation) {
        if (!activeEncryptExecutor) {
            return paramValue;
        }
        if (!encrypt) {
            return paramValue;
        }
        return AesUtil.encrypt(paramValue);
    }
    
    @Override
    public String encryptField(String fieldName, String fieldValue, Encrypt annotation, Object pojo) {
        if (!activeEncryptExecutor) {
            return fieldValue;
        }
        if (!encrypt) {
            return fieldValue;
        }
        return AesUtil.encrypt(fieldValue);
    }
    
    @Override
    public String decryptField(String fieldName, String fieldValue, Encrypt annotation, Object pojo) {
        if (!activeEncryptExecutor) {
            return fieldValue;
        }
        if (!decrypt) {
            return fieldValue;
        }
        return AesUtil.decrypt(fieldValue);
    }
}
