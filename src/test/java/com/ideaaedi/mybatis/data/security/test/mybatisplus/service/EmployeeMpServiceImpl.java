package com.ideaaedi.mybatis.data.security.test.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.mapper.EmployeeMpMapper;
import com.ideaaedi.mybatis.data.security.test.mybatisplus.model.EmployeeMp;
import org.springframework.stereotype.Service;

@Service
public class EmployeeMpServiceImpl extends ServiceImpl<EmployeeMpMapper, EmployeeMp> implements EmployeeMpService {

}
