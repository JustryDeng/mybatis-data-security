package com.ideaaedi.mybatis.data.security.annotation;

import com.ideaaedi.mybatis.data.security.config.MybatisDataSecurityConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 启动mybatis数据加解密
 *
 * @author JustryDeng
 * @since 2021/7/18 16:29:22
 */
@Inherited
@Documented
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
@ImportAutoConfiguration(value = {MybatisDataSecurityConfiguration.class})
public @interface EnableMybatisDataSecurity {
}
