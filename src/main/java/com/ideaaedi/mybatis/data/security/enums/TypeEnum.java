package com.ideaaedi.mybatis.data.security.enums;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.TypeUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * 对象类型枚举
 *
 * @author JustryDeng
 * @since 2021/2/10 22:30:56
 */
public enum TypeEnum {
    
    /** 基础类型或其包装类 */
    PRIMITIVE_OR_WRAPPER,
    
    /** 字符串 */
    STRING,
    
    /** map */
    MAP,
    
    /** collection */
    COLLECTION,
    
    /** array */
    ARRAY,
    
    /** 业务object-bean */
    CUSTOM_BEAN,
    
    /** 系统object-bean */
    SYSTEM_BEAN;
    
    /**
     * 解析clazz的数据类型
     *
     * @param clazz
     *            待解析的类
     * @return  clazz的类型
     */
    public static TypeEnum parseType(Class<?> clazz) {
        Objects.requireNonNull(clazz, "clazz cannot be null.");
        // 除了常见的八大基本类型外， void也处于基本类型的范畴
        if (ClassUtils.isPrimitiveOrWrapper(clazz)) {
            return PRIMITIVE_OR_WRAPPER;
        }
        if (TypeUtils.isAssignable(clazz, String.class)) {
            return STRING;
        }
        if (TypeUtils.isAssignable(clazz, Map.class)) {
            return MAP;
        }
        if (TypeUtils.isAssignable(clazz, Collection.class)) {
            return COLLECTION;
        }
        if (TypeUtils.isArrayType(clazz)) {
            return ARRAY;
        }
        // 这里认为以java开头的为系统bean，非这位业务bean
        String sysBeanPrefix = "java";
        if (clazz.getName().startsWith(sysBeanPrefix)) {
            return SYSTEM_BEAN;
        } else {
            return CUSTOM_BEAN;
        }
    }
}
