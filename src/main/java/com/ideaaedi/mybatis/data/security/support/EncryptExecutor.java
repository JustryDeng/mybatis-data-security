package com.ideaaedi.mybatis.data.security.support;

import com.ideaaedi.mybatis.data.security.annotation.Encrypt;
import org.springframework.lang.NonNull;

/**
 * 加解密执行器
 * <p>
 *     <b>说明：</b>
 *     <ul>
 *         <li>1. @Encrypt注解可运用于ElementType.FIELD和ElementType.PARAMETER， 所以这里加解密又分为两种情况</li>
 *         <li>2. @Encrypt运用于ElementType.PARAMETER时，只涉及加密，不涉及解密</li>
 *         <li>3. @Encrypt只能作用于String类型的变量或字段上，所以加解密都是针对字符串的</li>
 *     </ul>
 * </p>
 *
 * @author JustryDeng
 * @since 2021/2/10 22:42:22
 */
public interface EncryptExecutor {
    
    /**
     * 加密
     * <p>
     *     提示：同一个明文可能会调用多次加密，建议实现时考虑上缓存机制
     * </p>
     *
     * @param paramName
     *            参数前@Param指定的名称（，一定不为null）
     * @param paramValue
     *            待加密的字段值（，一定不为null）
     * @param annotation
     *            加密注解信息（，一定不为null）
     * @return  加密后的数据
     */
    String encryptParameter(@NonNull String paramName, @NonNull String paramValue, @NonNull Encrypt annotation);

    /**
     * 加密
     * <p>
     *     提示：同一个明文可能会调用多次加密，建议实现时考虑上缓存机制
     * </p>
     *
     * @param fieldName
     *            待加密的字段的字段名（，一定不为null）
     * @param fieldValue
     *            待加密的字段的字段值（，一定不为null）
     * @param annotation
     *            加密注解信息（，一定不为null）
     * @param pojo
     *            字段所在的当前对象（，一定不为null）
     * @return  加密后的数据
     */
    String encryptField(@NonNull String fieldName, @NonNull String fieldValue, Encrypt annotation, @NonNull Object pojo);
    
    /**
     * 解密
     *
     * @param fieldName
     *            待解密字段的字段名称（，一定不为null）
     * @param fieldValue
     *            待解密字段的值（，一定不为null）
     * @param annotation
     *            解密注解信息（，一定不为null）
     * @param pojo
     *            fieldValue对应所处的对象（，一定不为null）
     * @return  解密后的数据
     */
    String decryptField(@NonNull String fieldName, @NonNull String fieldValue, @NonNull Encrypt annotation, @NonNull Object pojo);
}
