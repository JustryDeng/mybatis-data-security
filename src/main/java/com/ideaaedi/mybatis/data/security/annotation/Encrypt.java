package com.ideaaedi.mybatis.data.security.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加密注解
 *
 * @author JustryDeng
 * @since 2021/2/8 23:28:49
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface Encrypt {
    
    /** 加密类型 */
    String type() default "AES";
    
    /** （同一个类中，）字段的加密解密顺序 */
    int order() default 0;
    
    /** 预留字段 */
    String ext() default "";
    
}
